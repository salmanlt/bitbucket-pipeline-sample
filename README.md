![LambdaTest Logo](https://www.lambdatest.com/static/images/logo.svg)

# LambdaTest - Bitbucket Pipelines

**This Bitbucket Pipeline sample seamlessly integrates LambdaTest Tunnel and runs Selenium test written in NigthwatchJS framework on LambdaTest Grid.**

## Setup

- Clone this repository:

  - git clone <https://bitbucket.org/lambdatest/bitbucket-pipeline-sample>

- Add the following environment variables to your Bitbucket repository:

  - `LAMBDATEST_USERNAME`: (Required) LambdaTest username
  - `LAMBDATEST_KEY`: (Required) LambdaTest accessKey

  Note: You can get these values from your [profile page](https://accounts.lambdatest.com/detail/profile).

## Example usage

```yaml
image: node:10.15.3

pipelines:
  default:
    - step:
        name: Run Sample Test Case
        caches:
          - node
        script:
          - npm install
          - npm run start:test:server
          - ./run_tunnel.sh # only needed when testing a locally hosted website
          - npm test
        after-script:
          # only needed when testing with tunnel
          - PORT=$(head -1 /tmp/port)
          - status_code=$(curl --request DELETE --write-out %{http_code} --silent --output /dev/null http://127.0.0.1:$PORT/api/v1.0/stop)
          - if [[ "$status_code" -ne 200 ]]; then  pkill LT; fi
          - rm /tmp/port
```

1. A node docker image is used and the dependencies are installed.
2. A node http-server is started on port 8888 that servers the [todo app](assets/index.html)
3. To enable LambdaTest browsers and devices access the local server the LambdaTest Tunnel is downloaded, extracted and executed so as to establish a secure connection to `LambdaTest Tunnel Servers`
4. It then runs a [NightwatchJS](tests/todo_test.js) test on LambdaTest Selenium grid.
5. After the script is executed the tunnel client is closed

## About LambdaTest
[LambdaTest](https://www.lambdatest.com/) is a cloud based selenium grid infrastructure that can help you run automated cross browser compatibility tests on 2000+ different browser and operating system environments. LambdaTest supports all programming languages and frameworks that are supported with Selenium, and have easy integrations with all popular CI/CD platforms. It's a perfect solution to bring your [selenium automation testing](https://www.lambdatest.com/selenium-automation) to cloud based infrastructure that not only helps you increase your test coverage over multiple desktop and mobile browsers, but also allows you to cut down your test execution time by running tests on parallel.
Salman Khan
SALMAN KHAN